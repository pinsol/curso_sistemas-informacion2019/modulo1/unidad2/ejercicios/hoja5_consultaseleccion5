﻿/** MODULO I - UNIDAD II - CONSULTAS DE SELECCION 5 **/

USE ciclistas;

/* 1.1 Nombre y edad de los ciclistas que han NO ganado etapas */
SELECT DISTINCT
  c.nombre,
  c.edad 
FROM
  ciclista c
   LEFT JOIN etapa e 
   ON c.dorsal = e.dorsal 
WHERE
  e.dorsal IS NULL
;

/* 1.2 Nombre y edad de los ciclistas que NO han ganado puertos */
SELECT DISTINCT
  c.nombre,
  c.edad
FROM
  ciclista c
   LEFT JOIN puerto p
   ON c.dorsal = p.dorsal
WHERE
  p.dorsal IS NULL
;

/* 1.3  Listar el director de los equipos que tengan ciclistas que NO hayan ganado NINGUNA etapa */
SELECT DISTINCT
  director
FROM
  ciclista c
  LEFT JOIN equipo e USING (nomequipo)
  LEFT JOIN etapa e1 USING (dorsal)
WHERE
  e1.dorsal IS NULL
;

/* 1.4 Dorsal y nombre de los ciclistas que NO hayan llevado algún maillot */
SELECT DISTINCT
  c.dorsal,
  c.nombre
FROM
  ciclista c
  LEFT JOIN lleva l USING (dorsal)
WHERE
  l.dorsal IS NULL
; 

/* 1.5 Dorsal y nombre de los ciclistas que NO hayan llevado el maillot amarillo NUNCA */
SELECT DISTINCT
  c.dorsal,
  c.nombre
FROM ciclista c LEFT JOIN (SELECT
    l.dorsal
  FROM lleva l
  WHERE l.código = (SELECT
      m.código
    FROM maillot m
    WHERE m.color LIKE 'amarillo')) c1 USING (dorsal)
WHERE
  c1.dorsal IS NULL
;

/* 1.6 Indicar el numetapa de las etapas que NO tengan puertos */
SELECT
  e.numetapa
FROM
  etapa e
  LEFT JOIN puerto p USING (numetapa)
WHERE p.numetapa IS NULL
  ORDER BY e.numetapa
;

/* 1.7 Indicar la distancia media de las etapas que NO tengan puertos */
SELECT AVG(e.kms) mediaKms
FROM
  etapa e
  LEFT JOIN puerto p USING (numetapa)
WHERE p.numetapa IS NULL
;

/* 1.8 Listar el número de ciclistas que NO hayan ganado alguna etapa */
SELECT
  COUNT(c.dorsal) nPerdedoresEtapa
FROM
  ciclista c
  LEFT JOIN etapa e USING (dorsal)
WHERE
  e.dorsal IS NULL
;

/* 1.9 Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto */
SELECT DISTINCT
  c.dorsal
FROM
  etapa e
  JOIN ciclista c USING (dorsal)
  LEFT JOIN puerto p USING (numetapa)
WHERE
  p.numetapa IS NULL
;

/* 1.10 Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos */
SELECT DISTINCT
  c.dorsal
FROM
  ciclista c
  JOIN etapa e USING (dorsal)
  LEFT JOIN puerto p USING (numetapa)
WHERE
  p.numetapa IS NULL
;